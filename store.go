package slog

import (
	"sync"
)

// newStore initialises a new templateWriterStore.
func newStore() *templateWriterStore {
	return &templateWriterStore{
		store:  make(map[string]*storeItem),
		scPool: sync.Pool{New: func() interface{} { return newScanner() }},
	}
}

type templateWriterStore struct {
	store  map[string]*storeItem
	mux    sync.RWMutex
	scPool sync.Pool
}

type storeItem struct {
	writer *templateWriter
}

func (ts *templateWriterStore) get(template string) *storeItem {
	ts.mux.RLock()
	if si, ok := ts.store[template]; ok {
		ts.mux.RUnlock()
		return si
	}
	ts.mux.RUnlock()
	return ts.parseTemplate(template)
}

func (ts *templateWriterStore) parseTemplate(template string) *storeItem {
	s := ts.scPool.Get().(*scanner)
	defer ts.scPool.Put(s)
	s.reset()

	var writer templateWriter
	var item storeItem

	if tokenInfos, err := s.parse(template); err == nil {

		tokens := make([]tokenInfo, 0, len(tokenInfos))

		writer = templateWriter{tokenInfos: append(tokens, tokenInfos...), template: template}
		item = storeItem{writer: &writer}
	} else {
		writer = templateWriter{tokenInfos: []tokenInfo{tokenInfo{text: template, typ: tokenTypText}}, template: template}
		item = storeItem{writer: &writer}
	}

	ts.mux.Lock()
	defer ts.mux.Unlock()

	if si, ok := ts.store[template]; ok {
		return si
	}
	ts.store[template] = &item
	return &item
}
