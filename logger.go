package slog

import (
	"context"
	"time"

	"bitbucket.org/go-sharp/slog/shared"
)

var _ Logger = &logger{}

type logger struct {
	cfg   *config
	store *templateWriterStore
	props []shared.Property
}

func newLogger(cfg config) *logger {
	return &logger{
		store: newStore(),
		props: make([]shared.Property, 0, 10),
		cfg:   &cfg,
	}
}

func (l *logger) Verbose(msg string, props ...interface{}) {
	l.logEvent(context.Background(), shared.VerboseLevel, msg, props...)
}

func (l *logger) Debug(msg string, props ...interface{}) {
	l.logEvent(context.Background(), shared.DebugLevel, msg, props...)
}

func (l *logger) Info(msg string, props ...interface{}) {
	l.logEvent(context.Background(), shared.InfoLevel, msg, props...)
}

func (l *logger) Warn(msg string, props ...interface{}) {
	l.logEvent(context.Background(), shared.WarnLevel, msg, props...)
}

func (l *logger) Error(msg string, props ...interface{}) {
	l.logEvent(context.Background(), shared.ErrorLevel, msg, props...)
}

func (l *logger) Fatal(msg string, props ...interface{}) {
	l.logEvent(context.Background(), shared.FatalLevel, msg, props...)
}

func (l *logger) WithProperty(props ...shared.Property) Logger {
	// Important if logger is used in different goroutines,
	// otherwise we overwrite the underlying items.
	p := make([]shared.Property, 0, len(l.props)+len(props))
	p = append(p, l.props...)
	nl := &logger{
		cfg:   l.cfg,
		store: l.store,
		props: append(p, props...),
	}
	return nl
}

func (l *logger) WithContext() ContextLogger {
	return ctxFunc(l.logEvent)
}

func (l *logger) WriteEvent(ctx context.Context, event shared.Event) {
	if event.Level < l.cfg.levelSwitch.Level() || l.cfg.levelSwitch.Level() == shared.NoneLevel {
		return
	}

	l.mergeProps(ctx, &event)

	for _, s := range l.cfg.sinks {
		s.WriteEvent(ctx, event)
	}
}

func (l *logger) mergeProps(ctx context.Context, event *shared.Event) {
	var props = make([]shared.Property, 0, (len(event.Properties)+len(l.cfg.properties))*2)
	var mm = make(map[string]struct{})

	for i := range event.Properties {
		p := event.Properties[i]
		if _, ok := mm[p.Name]; ok {
			continue
		}
		props = append(props, p)
		mm[p.Name] = struct{}{}
	}

	for i := range l.cfg.properties {
		p := l.cfg.properties[i]
		if _, ok := mm[p.Name]; ok {
			continue
		}
		props = append(props, p)
		mm[p.Name] = struct{}{}
	}

	for i := range l.props {
		p := l.props[i]
		if _, ok := mm[p.Name]; ok {
			continue
		}
		props = append(props, p)
		mm[p.Name] = struct{}{}
	}

	for _, e := range l.cfg.enrichers {
		e.Enrich(ctx, func(p shared.Property) {
			if _, ok := mm[p.Name]; ok {
				return
			}
			props = append(props, p)
			mm[p.Name] = struct{}{}
		})
	}
	event.Properties = props
}

func (l *logger) logEvent(ctx context.Context, lvl shared.Level, msg string, props ...interface{}) {
	var properties []shared.Property
	si := l.store.get(msg)

	tokens := si.writer.Props()
	count := len(tokens)
	if len(props) < count {
		for i := 0; i < count-len(props); i++ {
			props = append(props, nil)
		}
	}

	for i := 0; i < count; i++ {
		token := tokens[i]
		p := shared.Property{
			Name:      token.name,
			Serialize: token.serialize,
			Format:    token.format,
			Alignment: token.alignment,
		}
		if i < len(props) {
			p.Value = props[i]
		}
		properties = append(properties, p)
	}

	if len(props) > count {
		p := shared.Property{
			Name:  "Unused values",
			Value: props[count:],
		}
		properties = append(properties, p)
	}

	l.WriteEvent(ctx, shared.Event{
		Date:       time.Now(),
		Level:      lvl,
		Renderer:   si.writer,
		Properties: properties,
	})

}

type ctxFunc func(ctx context.Context, lvl shared.Level, msg string, props ...interface{})

func (cf ctxFunc) Verbose(ctx context.Context, msg string, props ...interface{}) {
	cf(ctx, shared.VerboseLevel, msg, props...)
}

func (cf ctxFunc) Debug(ctx context.Context, msg string, props ...interface{}) {
	cf(ctx, shared.DebugLevel, msg, props...)
}

func (cf ctxFunc) Info(ctx context.Context, msg string, props ...interface{}) {
	cf(ctx, shared.InfoLevel, msg, props...)
}

func (cf ctxFunc) Warn(ctx context.Context, msg string, props ...interface{}) {
	cf(ctx, shared.WarnLevel, msg, props...)
}

func (cf ctxFunc) Error(ctx context.Context, msg string, props ...interface{}) {
	cf(ctx, shared.ErrorLevel, msg, props...)
}

func (cf ctxFunc) Fatal(ctx context.Context, msg string, props ...interface{}) {
	cf(ctx, shared.FatalLevel, msg, props...)
}
