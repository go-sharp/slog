package slog

import (
	"reflect"
	"regexp"
	"testing"
)

func Test_scannerRun(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name       string
		args       args
		wantTokens []tokenInfo
		wantErr    bool
	}{
		{
			name: "Two simple properties",
			args: args{s: "User {User} logged in from {IP}"},
			wantTokens: []tokenInfo{
				{text: "User ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "User"},
				{text: " logged in from ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "IP"},
			},
		},
		{
			name: "One serializable and one normal property",
			args: args{s: "User {@User} logged in from {$IP}"},
			wantTokens: []tokenInfo{
				{text: "User ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "User", serialize: true},
				{text: " logged in from ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "IP", serialize: false},
			},
		},
		{
			name: "Property with alignment",
			args: args{s: "User {@User,489} logged in!"},
			wantTokens: []tokenInfo{
				{text: "User ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "User", serialize: true, alignment: "489"},
				{text: " logged in!", typ: tokenTypText},
			},
		},
		{
			name: "Property with format",
			args: args{s: "User {@User:##00$$@@2\"} logged in!"},
			wantTokens: []tokenInfo{
				{text: "User ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "User", serialize: true, format: "##00$$@@2\""},
				{text: " logged in!", typ: tokenTypText},
			},
		},
		{
			name: "Property with format and alignment",
			args: args{s: "User {@User,-489:##00$$@@2\"} logged in!"},
			wantTokens: []tokenInfo{
				{text: "User ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "User", serialize: true, format: "##00$$@@2\"", alignment: "-489"},
				{text: " logged in!", typ: tokenTypText},
			},
		},
		{
			name: "Only a property",
			args: args{s: "{User}"},
			wantTokens: []tokenInfo{
				{typ: tokenTypProperty, name: "User", serialize: false},
			},
		},
		{
			name: "Property with _ in name",
			args: args{s: "{User_name}"},
			wantTokens: []tokenInfo{
				{typ: tokenTypProperty, name: "User_name", serialize: false},
			},
		},
		{
			name: "One property and one escaped sequence",
			args: args{s: "User {User} logged in from {{IP}"},
			wantTokens: []tokenInfo{
				{text: "User ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "User", serialize: false},
				{text: " logged in from {IP}", typ: tokenTypText},
			},
		},
		{
			name:    "Property with missing brace",
			args:    args{s: "User {User"},
			wantErr: true,
		},
		{
			name:    "Property with invalid alignment",
			args:    args{s: "{User,--498}"},
			wantErr: true,
		},
		{
			name:    "After opening brace EOF",
			args:    args{s: "{"},
			wantErr: true,
		},
		{
			name:    "After opening brace follows closing brace",
			args:    args{s: "{}"},
			wantErr: true,
		},
		{
			name:    "After opening brace follows :",
			args:    args{s: "{:"},
			wantErr: true,
		},
		{
			name:    "After serialize rune follows :",
			args:    args{s: "{@:"},
			wantErr: true,
		},
		{
			name:    "Invalid property name",
			args:    args{s: "{user-}"},
			wantErr: true,
		},
		{
			name:    "Invalid first alignment rune",
			args:    args{s: "{user,a}"},
			wantErr: true,
		},
		{
			name:    "Invalid alignment rune EOF",
			args:    args{s: "{user,4"},
			wantErr: true,
		},
		{
			name:    "Invalid second alignment rune",
			args:    args{s: "{user,4a"},
			wantErr: true,
		},
		{
			name:    "Invalid first format rune {",
			args:    args{s: "{user,45:{}"},
			wantErr: true,
		},
		{
			name:    "Invalid first format rune }",
			args:    args{s: "{user,45:}"},
			wantErr: true,
		},
		{
			name:    "Invalid first format rune < 0x1f",
			args:    args{s: "{user,45:" + string(0x1a) + "}"},
			wantErr: true,
		},
		{
			name:    "Invalid second format rune {",
			args:    args{s: "{user,45:ad{}"},
			wantErr: true,
		},
		{
			name:    "Invalid first format rune < 0x1f",
			args:    args{s: "{user,45:as" + string(0x1a) + "}"},
			wantErr: true,
		},
		{
			name: "Properties get reseted correctly",
			args: args{s: "User {@User,111:asd} logged in from {IP}"},
			wantTokens: []tokenInfo{
				{text: "User ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "User", serialize: true, alignment: "111", format: "asd"},
				{text: " logged in from ", typ: tokenTypText},
				{typ: tokenTypProperty, name: "IP", serialize: false},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			scanner := newScanner()
			got, err := scanner.parse(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("scanner.parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if len(got) != len(tt.wantTokens) {
				t.Errorf("scanner.parse() len(tokenInfo) = %v, want %v", len(got), len(tt.wantTokens))
				return
			}

			for i := 0; i < len(got); i++ {
				if got[i].isTextToken() != tt.wantTokens[i].isTextToken() {
					t.Errorf("scanner.parse() got = '%v', want '%v'",
						got[i].isTextToken(), tt.wantTokens[i].isTextToken())
					return
				}

				if !reflect.DeepEqual(got[i], tt.wantTokens[i]) {
					t.Errorf("logger.logEvent got level = %v, want %v", got[i], tt.wantTokens[i])
					return
				}
				// if !got[i].isTextToken() {
				// 	continue
				// }

				// if got[i].text != tt.wantTokens[i].text {
				// 	t.Errorf("scanner.parse() got = '%v', want '%v'",
				// 		got[i].text, tt.wantTokens[i].text)
				// }
			}

		})
	}
}

func Test_resetScanner(t *testing.T) {
	s := newScanner()
	s.parse("User {@User} logged in from {$IP}. Hello {Momo,-23:#*//}! Call {{muh}")

	s.reset()

	if len(s.tokens) != 0 {
		t.Errorf("scanner.reset() got s.tokens = '%v', want '%v'", len(s.tokens), 0)
	}
	var tokInfo = tokenInfo{}
	if s.curTok != tokInfo {
		t.Errorf("scanner.reset() got s.tokens = '%v', want '%v'", len(s.tokens), 0)
	}

	if s.buf.Len() != 0 {
		t.Errorf("scanner.reset() got s.buf.Len() = '%v', want '%v'", s.buf.Len(), 0)
	}

	if s.c != 0 {
		t.Errorf("scanner.reset() got s.c = '%v', want '%v'", s.c, 0)
	}

	if s.cur != 0 {
		t.Errorf("scanner.reset() got s.cur = '%v', want '%v'", s.cur, 0)
	}

	if s.err != nil {
		t.Errorf("scanner.reset() got s.err = '%v', want '%v'", s.err, "")
	}

	if s.state != parseStateStart {
		t.Errorf("scanner.reset() got s.fn = '%v', want '%v'", s.state, parseStateStart)
	}

	if s.n != 0 {
		t.Errorf("scanner.reset() got s.n = '%v', want '%v'", s.n, 0)
	}

	if s.pos != 0 {
		t.Errorf("scanner.reset() got s.pos = '%v', want '%v'", s.pos, 0)
	}

	if s.start != 0 {
		t.Errorf("scanner.reset() got s.start = '%v', want '%v'", s.start, 0)
	}

	if s.tok != tokEOF {
		t.Errorf("scanner.reset() got s.tok = '%v', want '%v'", s.tok, 0)
	}

}

func Test_print(t *testing.T) {
	s := newScanner()
	tok, err := s.parse("User {@User} logged in from {$IP}. Hello {Momo,-23:#*//}! Call {{muh}")
	if err != nil {
		t.Fatal(err)
	}
	t.Log("Tokens:")
	for _, to := range tok {
		t.Log(to)
	}
}

func BenchmarkScanner(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		t := newScanner()
		t.parse("User {User} logged in from {IP} soasd as {$asd:asddf} asdda {@asd,345:wer}")
	}
}

func BenchmarkParseWithRegex(b *testing.B) {
	b.ReportAllocs()
	re, _ := regexp.Compile("{(@|$)?[A-z]+(,-?[0-9]+)?(:[A-z0-9]+)}")
	for i := 0; i < b.N; i++ {
		re.FindAllString("User {User} logged in from {IP} soasd as {$asd:asddf} asdda {@asd,345:wer}", -1)
	}
}
