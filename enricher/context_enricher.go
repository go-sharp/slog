package enricher

import (
	"context"
	"errors"
	"regexp"

	"bitbucket.org/go-sharp/slog"
	"bitbucket.org/go-sharp/slog/shared"
)

var propMatcher *regexp.Regexp

func init() {
	propMatcher = regexp.MustCompile("^(@|$)?([a-zA-Z0-9_]+)((,)([-]?[0-9]+))?((:)(.+))?$")
}

type contextKey string

const ctxKey contextKey = "ContextEnricherKey"

type keyMap map[string]struct {
	key  interface{}
	prop *shared.Property
}

// NewContextEnricher creates a new context enricher.
func NewContextEnricher() *ContextEnricher {
	return &ContextEnricher{km: make(keyMap)}
}

// ContextEnricher adds properties form the current log context.
type ContextEnricher struct {
	km keyMap
}

// Add adds a new property to the enricher. It takes a property name with configuration flags
// like in the message templates. For example: "User", "@User", "$User", "User,-123", "User:##0ad"
// or any combination. Second parameter is the key that is used to identify the value in the
// current log context, which will be logged with the specified name.
func (c *ContextEnricher) Add(name string, key interface{}) error {
	if key == nil {
		return errors.New("key must not be nil")
	}

	p, err := getProperty(name)
	if err != nil {
		return err
	}

	c.km[p.Name] = struct {
		key  interface{}
		prop *shared.Property
	}{
		key:  key,
		prop: p,
	}

	return nil
}

// Enrich satisfies the Enricher interface.
func (c *ContextEnricher) Enrich(ctx context.Context, appender slog.PropertyAppender) {
	if ctx == nil {
		return
	}
	for i := range c.km {
		p := c.km[i]
		if v := ctx.Value(p.key); v != nil {
			appender(shared.Property{
				Name:      p.prop.Name,
				Alignment: p.prop.Alignment,
				Format:    p.prop.Format,
				Serialize: p.prop.Serialize,
				Value:     v,
			})
		}
	}
}

func getProperty(s string) (*shared.Property, error) {
	p := shared.Property{}
	if s == "" {
		return nil, errors.New("empty string is not valid")
	}

	matches := propMatcher.FindAllStringSubmatch(s, 1)
	if len(matches) == 0 {
		return nil, errors.New("invalid input string found")
	}

	match := matches[0]
	// (@|$)
	if match[1] == "@" {
		p.Serialize = true
	}
	p.Name = match[2]      // ([a-zA-Z0-9_]+)
	p.Alignment = match[5] // ([-]?[0-9]+)
	p.Format = match[8]    // (.+)

	return &p, nil
}
