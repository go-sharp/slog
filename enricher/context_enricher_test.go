package enricher

import (
	"reflect"
	"testing"

	"bitbucket.org/go-sharp/slog/shared"
)

func Test_getProperty(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		want    *shared.Property
		wantErr bool
	}{
		{
			name: "Simple property",
			args: args{s: "User"},
			want: &shared.Property{
				Name: "User",
			},
			wantErr: false,
		},
		{
			name: "Property should be serialized",
			args: args{s: "@User"},
			want: &shared.Property{
				Name:      "User",
				Serialize: true,
			},
			wantErr: false,
		},
		{
			name: "Property with alignment",
			args: args{s: "User,1234"},
			want: &shared.Property{
				Name:      "User",
				Serialize: false,
				Alignment: "1234",
			},
			wantErr: false,
		},
		{
			name: "Property with negativ alignment",
			args: args{s: "User,-1234"},
			want: &shared.Property{
				Name:      "User",
				Serialize: false,
				Alignment: "-1234",
			},
			wantErr: false,
		},
		{
			name:    "Property with invalid format",
			args:    args{s: "User:"},
			wantErr: true,
		},
		{
			name: "Property with alignment and format",
			args: args{s: "@User,-1234:HH:mm"},
			want: &shared.Property{
				Name:      "User",
				Serialize: true,
				Alignment: "-1234",
				Format:    "HH:mm",
			},
			wantErr: false,
		},
		{
			name:    "Empty string",
			args:    args{s: ""},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getProperty(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("getProperty() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getProperty() = %v, want %v", got, tt.want)
			}
		})
	}
}
