package slog

import (
	"context"
	"fmt"
	"io"
	"sync"

	"bitbucket.org/go-sharp/slog/shared"
)

// Logger an interface to implement for all Logger types.
// Logger functions requires as first parameter a template string
// and for every placeholder an additional argument.
type Logger interface {
	shared.Sink
	Verbose(string, ...interface{})
	Debug(string, ...interface{})
	Info(string, ...interface{})
	Warn(string, ...interface{})
	Error(string, ...interface{})
	Fatal(string, ...interface{})
	WithProperty(...shared.Property) Logger
	WithContext() ContextLogger
}

// ContextLogger accepts a context for request based logging.
type ContextLogger interface {
	Verbose(context.Context, string, ...interface{})
	Debug(context.Context, string, ...interface{})
	Info(context.Context, string, ...interface{})
	Warn(context.Context, string, ...interface{})
	Error(context.Context, string, ...interface{})
	Fatal(context.Context, string, ...interface{})
}

// SinkFunc wraps a single function into a type
// that implements the Sink interface.
type SinkFunc func(context.Context, shared.Event)

// WriteEvent passes an event to the underlying function.
func (s SinkFunc) WriteEvent(ctx context.Context, e shared.Event) {
	s(ctx, e)
}

// NewLevelSwitch creates a new level switch.
func NewLevelSwitch(lvl shared.Level) LevelSwitch {
	return &levelSwitch{mux: new(sync.RWMutex), lvl: lvl}
}

// LevelSwitch controls which minimum level should be logged.
type LevelSwitch interface {
	Level() shared.Level
	SetLevel(shared.Level)
}

type levelSwitch struct {
	mux *sync.RWMutex
	lvl shared.Level
}

func (l *levelSwitch) Level() shared.Level {
	l.mux.RLock()
	defer l.mux.RUnlock()
	return l.lvl
}

func (l *levelSwitch) SetLevel(lvl shared.Level) {
	l.mux.Lock()
	defer l.mux.Unlock()
	l.lvl = lvl
}

type templateWriter struct {
	template   string
	tokenInfos []tokenInfo
}

type tokenTyp uint8

const (
	tokenTypText tokenTyp = iota
	tokenTypProperty
)

type tokenInfo struct {
	text      string
	typ       tokenTyp
	format    string
	name      string
	alignment string
	serialize bool
}

func (t tokenInfo) isTextToken() bool {
	if t.typ == tokenTypText {
		return true
	}
	return false
}

func (tw templateWriter) Template() string {
	return tw.template
}

func (tw templateWriter) Count() int {
	count := 0
	for _, t := range tw.tokenInfos {
		if !t.isTextToken() {
			count++
		}
	}
	return count
}

func (tw templateWriter) Props() []tokenInfo {
	tokens := make([]tokenInfo, 0, len(tw.tokenInfos))
	for i := range tw.tokenInfos {
		if !tw.tokenInfos[i].isTextToken() {
			tokens = append(tokens, tw.tokenInfos[i])
		}
	}
	return tokens
}

func (tw templateWriter) Render(w io.Writer, props ...shared.Property) error {
	return tw.RenderF(w, shared.Formatter(shared.JSONFormat), props...)
}

func (tw templateWriter) RenderF(writer io.Writer, f shared.PropertyFormatter, props ...shared.Property) error {
	idx := 0
	w := proxyWriter{w: writer}
	for _, t := range tw.tokenInfos {
		if t.isTextToken() {
			w.Write(t.text)
			continue
		}
		if idx < len(props) {
			w.Write(f.Format(props[idx]))
			idx++
		}
	}

	for idx < len(props) {
		w.Write(fmt.Sprintf(" %v: %v", props[idx].Name, f.Format(props[idx])))
		idx++
	}
	return w.err
}

// Writes to the underliying writer as long as there is no error
type proxyWriter struct {
	w   io.Writer
	err error
}

func (w *proxyWriter) Write(s string) {
	if w.err == nil {
		_, w.err = w.w.Write([]byte(s))
	}
}
