package slog

import (
	"bytes"
	"testing"

	"bitbucket.org/go-sharp/slog/shared"
)

func Test_templateWriter_Write(t *testing.T) {
	type fields struct {
		template   string
		tokenInfos []tokenInfo
	}

	type args struct {
		props []shared.Property
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantW      string
		wantErr    bool
		wantTokens int
		wantCount  int
	}{
		{
			name: "Render template",
			fields: fields{
				template: "User {User} logged in from {IP}",
				tokenInfos: []tokenInfo{
					tokenInfo{text: "User ", typ: tokenTypText},
					tokenInfo{typ: tokenTypProperty},
					tokenInfo{text: " logged in from ", typ: tokenTypText},
					tokenInfo{typ: tokenTypProperty},
				},
			},
			args: args{
				props: []shared.Property{
					{Name: "User", Value: "jbond"},
					{Name: "IP", Value: "127.0.0.1"},
				},
			},
			wantW:      "User jbond logged in from 127.0.0.1",
			wantErr:    false,
			wantTokens: 4,
			wantCount:  2,
		},
		{
			name: "Render template with default serializer",
			fields: fields{
				template: "User {User} logged in from {IP}",
				tokenInfos: []tokenInfo{
					tokenInfo{text: "User ", typ: tokenTypText},
					tokenInfo{typ: tokenTypProperty},
					tokenInfo{text: " logged in from ", typ: tokenTypText},
					tokenInfo{typ: tokenTypProperty},
				},
			},
			args: args{
				props: []shared.Property{
					{Name: "User", Value: struct {
						Login     string
						FullName  string
						Age       int
						isPremium bool
					}{
						Login:     "jbond",
						FullName:  "James Bond",
						Age:       42,
						isPremium: true,
					},
						Serialize: true},
					{Name: "IP", Value: "127.0.0.1"},
				},
			},
			wantW:      "User {\"Login\":\"jbond\",\"FullName\":\"James Bond\",\"Age\":42} logged in from 127.0.0.1",
			wantErr:    false,
			wantTokens: 4,
			wantCount:  2,
		},
		{
			name: "Render template with more props",
			fields: fields{
				template: "User {User} logged in from {IP}",
				tokenInfos: []tokenInfo{
					tokenInfo{text: "User ", typ: tokenTypText},
					tokenInfo{typ: tokenTypProperty},
					tokenInfo{text: " logged in from ", typ: tokenTypText},
					tokenInfo{typ: tokenTypProperty},
				},
			},
			args: args{
				props: []shared.Property{
					{Name: "User", Value: "jbond"},
					{Name: "IP", Value: "127.0.0.1"},
					{Name: "ID", Value: "uniqueId"},
					{Name: "Level", Value: "Error"},
				},
			},
			wantW:      "User jbond logged in from 127.0.0.1 ID: uniqueId Level: Error",
			wantErr:    false,
			wantTokens: 4,
			wantCount:  2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tw := templateWriter{
				template:   tt.fields.template,
				tokenInfos: tt.fields.tokenInfos,
			}

			if len(tw.tokenInfos) != tt.wantTokens {
				t.Errorf("templateWriter.Write() tokenInfos = %v, wantTokens %v", len(tw.tokenInfos), tt.wantTokens)
				return
			}

			if tw.Count() != tt.wantCount {
				t.Errorf("templateWriter.Write() tokenInfos.Count = %v, wantCount %v", tw.Count(), tt.wantCount)
				return
			}

			w := &bytes.Buffer{}
			if err := tw.Render(w, tt.args.props...); (err != nil) != tt.wantErr {
				t.Errorf("templateWriter.Write() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotW := w.String(); gotW != tt.wantW {
				t.Errorf("templateWriter.Write() = %v, want %v", gotW, tt.wantW)
			}
		})
	}
}
