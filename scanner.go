package slog

import (
	"bytes"
	"fmt"
	"io"
)

const (
	tokTxt tok = iota
	tokLcbrace
	tokRcbrace
	tokComma
	tokColon
	tokAt
	tokDollar
	tokEOF
)

type parseState uint8

const (
	parseStateEOF parseState = iota
	parseStateStart
	parseStateOpenToken
	parseStatePropIDTokenStart
	parseStatePropIDToken
	parseStateFormatTokenStart
	parseStateFormatToken
	parseStateAlignmentTokenStart
	parseStateAlignmentToken
)

type scannerFunc func()
type tok int
type scanner struct {
	content    []byte
	tokens     []tokenInfo
	curTok     tokenInfo
	start, cur int
	n, pos     int
	c          rune
	tok        tok
	err        error
	r          *bytes.Reader
	state      parseState
	buf        *bytes.Buffer
}

// newScanner initialises a new scanner.
func newScanner() *scanner {
	scan := &scanner{
		tokens: make([]tokenInfo, 0, 20),
		buf:    bytes.NewBuffer(make([]byte, 0, 50)),
	}

	return scan
}

// parse parses a template string, it can be reused if reset is called before
// a sequential call to parse.
func (s *scanner) parse(template string) ([]tokenInfo, error) {
	s.content = []byte(template)
	s.r = bytes.NewReader(s.content)

	s.state = parseStateStart
	for s.state != parseStateEOF && s.err == nil {
		switch s.state {
		case parseStateStart:
			s.textToken()
		case parseStateOpenToken:
			s.openToken()
		case parseStatePropIDToken:
			fallthrough
		case parseStatePropIDTokenStart:
			s.propIDToken()
		case parseStateFormatTokenStart:
			fallthrough
		case parseStateFormatToken:
			s.formatToken()
		case parseStateAlignmentTokenStart:
			fallthrough
		case parseStateAlignmentToken:
			s.alignmentToken()

		}
	}

	if s.err != nil {
		return nil, s.err
	}

	return s.tokens, nil
}

func (s *scanner) textToken() {
	s.tokenize()

	if s.err != nil {
		return
	}

	switch s.tok {
	case tokTxt:
		// Do nothing read next rune.
	case tokLcbrace:
		s.state = parseStateOpenToken
	case tokEOF:
		if s.start < s.cur {
			s.buf.Write(s.content[s.start:s.cur])
		}
		if s.buf.Len() > 0 {
			s.tokens = append(s.tokens, tokenInfo{text: s.buf.String(), typ: tokenTypText})
		}
		s.state = parseStateEOF
	}
}

func (s *scanner) openToken() {
	s.tokenize()

	if s.err != nil {
		return
	}

	switch s.tok {
	case tokLcbrace:
		s.buf.Write(s.content[s.start : s.cur-1])
		s.start = s.cur
		s.state = parseStateStart
	case tokAt:
		s.handleTextToken()
		s.curTok = tokenInfo{typ: tokenTypProperty, serialize: true}
		s.state = parseStatePropIDToken
	case tokDollar:
		s.handleTextToken()
		s.curTok = tokenInfo{typ: tokenTypProperty, serialize: false}
		s.state = parseStatePropIDToken
	default:
		if s.isValidIdentifier() {
			if s.start <= s.cur {
				s.buf.Write(s.content[s.start : s.cur-2])
			}
			s.start = s.cur - 1
			if s.buf.Len() > 0 {
				s.tokens = append(s.tokens, tokenInfo{text: s.buf.String(), typ: tokenTypText})
				s.buf.Reset()
			}

			s.curTok = tokenInfo{typ: tokenTypProperty, serialize: false}
			s.state = parseStatePropIDToken
		} else {
			s.err = fmt.Errorf("Expected runes '{ @ $ [alphanum]' at pos %v, got = %v", s.pos, string(s.c))
			return
		}
	}
}

func (s *scanner) propIDToken() {
	s.tokenize()

	if s.err != nil {
		return
	}

	if s.state == parseStatePropIDToken {
		switch {
		case s.tok == tokRcbrace:
			s.handleIDToken()
			s.tokens = append(s.tokens, s.curTok)
			s.state = parseStateStart
		case s.tok == tokColon:
			s.handleIDToken()
			s.state = parseStateFormatTokenStart
		case s.tok == tokComma:
			s.handleIDToken()
			s.state = parseStateAlignmentTokenStart
		case s.isValidIdentifier():
			// Do nothing read next rune, use the same function.
		default:
			s.err = fmt.Errorf("Expected runes '} , : [alphanum]' at pos %v, got = %v", s.pos, string(s.c))
		}
	} else {

		switch {
		case s.isValidIdentifier():
			// Do nothing read next rune, use the same function.
			s.state = parseStatePropIDToken
		default:
			s.err = fmt.Errorf("Expected runes '[alphanum]' at pos %v, got = %v", s.pos, string(s.c))
		}
	}
}

func (s *scanner) alignmentToken() {
	s.tokenize()

	if s.err != nil {
		return
	}

	if s.state == parseStateAlignmentToken {

		switch {
		case s.c >= '0' && s.c <= '9':
			// Do nothing read next rune, use the same function.
		case s.tok == tokRcbrace:
			s.curTok.alignment = string(s.content[s.start : s.cur-1])
			s.tokens = append(s.tokens, s.curTok)
			s.start = s.cur
			s.state = parseStateStart
		case s.tok == tokColon:
			s.curTok.alignment = string(s.content[s.start : s.cur-1])
			s.start = s.cur
			s.state = parseStateFormatTokenStart
		default:
			s.err = fmt.Errorf("Expected runes '} : [num]' at pos %v, got = %v", s.pos, string(s.c))
		}
	} else {

		switch {
		case s.c == '-':
			// Do nothing read next rune, use the same function.
			fallthrough
		case s.c >= '0' && s.c <= '9':
			// Do nothing read next rune, use the same function.
			s.state = parseStateAlignmentToken
		default:
			s.err = fmt.Errorf("Expected runes '- [num]' at pos %v, got = %v", s.pos, string(s.c))
		}
	}
}

func (s *scanner) formatToken() {
	s.tokenize()

	if s.err != nil {
		return
	}

	if s.state == parseStateFormatToken {
		switch {
		case s.tok == tokRcbrace:
			s.curTok.format = string(s.content[s.start : s.cur-1])
			s.tokens = append(s.tokens, s.curTok)
			s.start = s.cur
			s.state = parseStateStart
		case s.tok != tokLcbrace && s.c > 0x1f:
			// Do nothing read next rune.
		default:
			s.err = fmt.Errorf("Expected runes ' } [rune] [^{}]' at pos %v, got = %v", s.pos, string(s.c))
		}
	} else {
		switch {
		case s.tok == tokTxt && s.c > 0x1f:
			// Do nothing read next rune.
			s.state = parseStateFormatToken
		default:
			s.err = fmt.Errorf("Expected runes '[alphanum]' at pos %v, got = %v", s.pos, string(s.c))
		}
	}
}

func (s *scanner) handleIDToken() {
	s.curTok.name = string(s.content[s.start : s.cur-1])
	s.start = s.cur
}

func (s *scanner) handleTextToken() {
	if s.start <= s.cur {
		s.buf.Write(s.content[s.start : s.cur-2])
	}

	s.start = s.cur
	if s.buf.Len() > 0 {
		s.tokens = append(s.tokens, tokenInfo{text: s.buf.String(), typ: tokenTypText})
		s.buf.Reset()
	}
}

func (s *scanner) tokenize() {
	s.c, s.n, s.err = s.r.ReadRune()
	s.pos++
	if s.err != nil {
		if s.err == io.EOF {
			s.err = nil
			s.tok = tokEOF
		}
		return
	}

	s.cur = s.cur + s.n
	switch s.c {
	case '{':
		s.tok = tokLcbrace
	case '}':
		s.tok = tokRcbrace
	case ',':
		s.tok = tokComma
	case ':':
		s.tok = tokColon
	case '$':
		s.tok = tokDollar
	case '@':
		s.tok = tokAt
	default:
		s.tok = tokTxt
	}
}

func (s *scanner) isValidIdentifier() bool {
	if s.c >= 'a' && s.c <= 'z' ||
		s.c >= 'A' && s.c <= 'Z' ||
		s.c >= '0' && s.c <= '9' ||
		s.c == '_' {
		return true
	}
	return false
}

func (s *scanner) reset() {
	s.buf.Reset()
	s.c, s.cur, s.err = 0, 0, nil
	s.state, s.n, s.pos = parseStateStart, 0, 0
	s.start, s.tok = 0, tokEOF
	s.tokens = s.tokens[0:0]
	s.curTok = tokenInfo{}
}
