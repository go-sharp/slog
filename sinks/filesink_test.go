package sinks

import (
	"context"
	"fmt"
	"io"
	"os"
	"testing"

	"bitbucket.org/go-sharp/slog/shared"
)

type testTW struct {
	template string
}

func (t *testTW) Template() string {
	return t.template
}

func (t *testTW) Count() int {
	return 0
}

func (t *testTW) Render(w io.Writer, props ...shared.Property) error {
	t.RenderF(w, shared.Formatter(shared.JSONFormat), props...)
	return nil
}

func (t *testTW) RenderF(w io.Writer, fn shared.PropertyFormatter, props ...shared.Property) error {
	fmt.Fprintf(w, "%v", t.template)
	return nil
}

func BenchmarkFileLogger(b *testing.B) {
	testCleanDir(testDataDir, nil)
	defer testCleanDir(testDataDir, nil)
	b.ReportAllocs()
	sink, _ := NewFileSink(fmt.Sprintf("%v%vlog.txt", testDataDir, string(os.PathSeparator)),
		FileSinkOptions{MaxRotate: 5, MaxSize: 1024 * 10, Compress: true})
	for i := 0; i < b.N; i++ {
		sink.WriteEvent(context.Background(), shared.Event{
			Level:    shared.InfoLevel,
			Renderer: &testTW{fmt.Sprintf("Hello iteration %v\n", i)},
		})
	}
}

func BenchmarkFileLoggerGo(b *testing.B) {
	testCleanDir(testDataDir, nil)
	defer testCleanDir(testDataDir, nil)
	b.ReportAllocs()
	sink, _ := NewFileSink(fmt.Sprintf("%v%vlog.txt", testDataDir, string(os.PathSeparator)),
		FileSinkOptions{MaxRotate: 5, MaxSize: 1024 * 10, Compress: true})
	for i := 0; i < b.N; i++ {
		go func(i int) {
			sink.WriteEvent(context.Background(), shared.Event{
				Level:    shared.InfoLevel,
				Renderer: &testTW{fmt.Sprintf("Hello iteration %v\n", i)},
			})
		}(i)
	}
}
