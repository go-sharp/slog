package sinks

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"strings"
	"sync"

	"bitbucket.org/go-sharp/slog/shared"
	"bitbucket.org/go-sharp/slog/term"
)

var _ shared.Sink = consoleSink{}

// NewConsoleSink returns a new console sink instance with the supplied options.
func NewConsoleSink(options ...ConsoleOpFunc) shared.Sink {
	opts := ConsoleOptions{useColor: true, printAll: false, printDate: true, printFormat: "15:04:05.000"}
	for _, fn := range options {
		fn(&opts)
	}
	logger := &consoleSink{options: opts, mux: new(sync.Mutex)}
	return logger
}

// ConsoleOpFunc configures the options for the Console Sink.
type ConsoleOpFunc func(*ConsoleOptions)

// UseConsoleColor enables or disables colored Loglevel output.
func UseConsoleColor(colored bool) ConsoleOpFunc {
	return func(o *ConsoleOptions) {
		o.useColor = colored
	}
}

// ExtendedConsoleOutput prints all additional properties to the output.
func ExtendedConsoleOutput(extend bool) ConsoleOpFunc {
	return func(o *ConsoleOptions) {
		o.printAll = extend
	}
}

// ConsolePrintDate if set to true, log entry will be prefixed with current date.
func ConsolePrintDate(printDate bool) ConsoleOpFunc {
	return func(o *ConsoleOptions) {
		o.printDate = printDate
	}
}

// ConsoleDateFormat sets the date format to use if print date is enabled.
func ConsoleDateFormat(format string) ConsoleOpFunc {
	return func(o *ConsoleOptions) {
		o.printFormat = format
	}
}

// ConsoleOptions are options for the ConsoleSink.
type ConsoleOptions struct {
	useColor    bool
	printAll    bool
	printDate   bool
	printFormat string
}

type consoleSink struct {
	mux     *sync.Mutex
	options ConsoleOptions
}

func (c consoleSink) WriteEvent(ctx context.Context, event shared.Event) {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.options.printDate {
		fmt.Fprintf(os.Stdout, "[%v] ", event.Date.Format(c.options.printFormat))
	}

	c.printColored(strings.ToUpper(fmt.Sprintf("%v:\t", event.Level)), event.Level)

	// Ensure a minimal distance between message and properties
	var buf = new(bytes.Buffer)
	event.Renderer.Render(buf, event.Properties[:event.Renderer.Count()]...)
	fmt.Print(buf.String(), fillString(buf.Bytes(), " ", 50))

	if c.options.printAll && len(event.Properties) > 0 {
		props := event.Properties[:]
		l := len(props)
		for i := 0; i < l; i++ {
			c.printColored(props[i].Name, event.Level)
			fmt.Fprint(os.Stdout, "=", shared.JSONFormat(props[i]))

			if i < l-1 {
				fmt.Fprint(os.Stdout, "|")
			}
		}

	}
	fmt.Fprintln(os.Stdout)
}

func (c consoleSink) printColored(msg string, lvl shared.Level) {
	if c.options.useColor {
		switch lvl {
		case shared.VerboseLevel:
			term.Blue.Print(msg)
		case shared.DebugLevel:
			term.Cyan.Print(msg)
		case shared.WarnLevel:
			term.Yellow.Print(msg)
		case shared.ErrorLevel:
			term.Red.Print(msg)
		case shared.FatalLevel:
			term.Magenta.Print(msg)
		default:
			term.Green.Print(msg)
		}
		return
	}
	fmt.Print(msg)
}
