package sinks

import (
	"archive/zip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"unicode/utf8"
)

// createPath creates a cleaned path from directory, filename and
// the file extension.
func createPath(path, name, ext string) string {
	if path == "" {
		path = "."
	}

	if name == "" && ext == "" {
		return filepath.Clean(path)
	}

	s := strings.Join([]string{path, name}, string(filepath.Separator))
	if ext != "" {
		s = fmt.Sprintf("%v.%v", s, ext)
	}
	return filepath.Clean(s)
}

// splitFilePath divides an absolute or relative file path
// into three parts, directory path, file name without extension
// and the extension of the file if exists.
func splitFilePath(fpath string) (path, name, ext string) {
	path = filepath.Dir(fpath)
	if path == "." {
		path = ""
	}
	parts := strings.Split(filepath.Base(fpath), ".")
	name = parts[0]

	if len(parts) == 2 {
		ext = parts[1]
	}
	return
}

// rotateFile takes a file path and the maximum amount of rotations
// to keep. It renames the file and appends an underscore and a number
// to the file name exp. log_1.txt.
func rotateFile(path string, maxRotate uint) error {
	f, err := os.OpenFile(path, os.O_RDWR, 0666)
	if err != nil {
		return err
	}
	f.Close()

	d, n, e := splitFilePath(path)

	var rotate func(uint) error
	rotate = func(current uint) error {
		if current >= maxRotate {
			return nil
		}

		target := createPath(d, fmt.Sprintf("%v_%v", n, current+1), e)
		src := createPath(d, fmt.Sprintf("%v_%v", n, current), e)
		if current == 0 {
			src = path
		}

		if _, err := os.Stat(target); err == nil {
			if err := rotate(current + 1); err != nil {
				return err
			}
		}

		return os.Rename(src, target)
	}

	return rotate(0)
}

// zipFile creates a zip archive of the file in the same
// directory with a .zip extension.
func zipFile(path string) (string, error) {
	dirPath, name, ext := splitFilePath(path)
	fullPath := createPath(dirPath, name, "zip")

	reader, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer reader.Close()

	fw, err := os.Create(fullPath)
	if err != nil {
		return "", err
	}
	defer fw.Close()

	zw := zip.NewWriter(fw)
	defer zw.Close()
	writer, err := zw.Create(fmt.Sprintf("%v.%v", name, ext))
	if err != nil {
		return "", err
	}

	_, err = io.Copy(writer, reader)
	if err != nil {
		return "", err
	}
	zw.Flush()
	return fullPath, nil
}

func getTmpFile(name, ext string) *os.File {
	tmpFile := fmt.Sprintf("%v%v%v_%v.%v", os.TempDir(), string(os.PathSeparator), name, os.Getpid(), ext)
	f, err := os.OpenFile(tmpFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)

	if err != nil {
		return nil
	}
	return f
}

func fillString(b []byte, spacer string, min int) string {
	rep := min - utf8.RuneCount(b)
	if rep > 0 {
		return strings.Repeat(spacer, rep)
	}
	return " "
}
