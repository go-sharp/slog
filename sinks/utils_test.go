package sinks

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

const (
	testDataDir = "testdata"
)

func Test_createPath(t *testing.T) {
	type args struct {
		path string
		name string
		ext  string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Absolute path",
			args{
				fmt.Sprintf("%[1]vroot%[1]vpath", string(filepath.Separator)),
				"log",
				"txt"},
			fmt.Sprintf("%[1]vroot%[1]vpath%[1]vlog.txt", string(filepath.Separator)),
		},
		{
			"Relative path without .",
			args{
				fmt.Sprintf("root%vpath", string(filepath.Separator)),
				"log",
				"txt"},
			fmt.Sprintf("root%[1]vpath%[1]vlog.txt", string(filepath.Separator)),
		},
		{
			"Relative path with .",
			args{
				fmt.Sprintf(".%[1]vroot%[1]vpath", string(filepath.Separator)),
				"log",
				"txt"},
			fmt.Sprintf("root%[1]vpath%[1]vlog.txt", string(filepath.Separator)),
		},
		{
			"Only file with ext",
			args{
				"",
				"log",
				"txt"},
			"log.txt",
		},
		{
			"Only file without ext",
			args{
				"",
				"log",
				""},
			"log",
		},
		{
			"Only ext",
			args{
				"",
				"",
				"txt"},
			".txt",
		},
		{
			"Only path",
			args{
				fmt.Sprintf(".%[1]vroot%[1]vpath", string(filepath.Separator)),
				"",
				""},
			fmt.Sprintf("root%[1]vpath", string(filepath.Separator)),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := createPath(tt.args.path, tt.args.name, tt.args.ext); got != tt.want {
				t.Errorf("createPath() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_splitFilePath(t *testing.T) {
	type args struct {
		fpath string
	}
	tests := []struct {
		name     string
		args     args
		wantPath string
		wantName string
		wantExt  string
	}{
		{
			"Absoluter path",
			args{fmt.Sprintf("%[1]vroot%[1]vpath%[1]vlog.txt", string(filepath.Separator))},
			fmt.Sprintf("%[1]vroot%[1]vpath", string(filepath.Separator)),
			"log",
			"txt",
		},
		{
			"Relative path with .",
			args{fmt.Sprintf(".%[1]vroot%[1]vpath%[1]vlog.txt", string(filepath.Separator))},
			fmt.Sprintf("root%[1]vpath", string(filepath.Separator)),
			"log",
			"txt",
		},
		{
			"Relative path without .",
			args{fmt.Sprintf("root%[1]vpath%[1]vlog.txt", string(filepath.Separator))},
			fmt.Sprintf("root%[1]vpath", string(filepath.Separator)),
			"log",
			"txt",
		},
		{
			"File without ext",
			args{fmt.Sprintf("%[1]vroot%[1]vpath%[1]vlog", string(filepath.Separator))},
			fmt.Sprintf("%[1]vroot%[1]vpath", string(filepath.Separator)),
			"log",
			"",
		},
		{
			"File without name",
			args{fmt.Sprintf("%[1]vroot%[1]vpath%[1]v.txt", string(filepath.Separator))},
			fmt.Sprintf("%[1]vroot%[1]vpath", string(filepath.Separator)),
			"",
			"txt",
		},
		{
			"Only file",
			args{"log.txt"},
			"",
			"log",
			"txt",
		},
		{
			"Only file without ext",
			args{"log."},
			"",
			"log",
			"",
		},
		{
			"Only with ext",
			args{".txt"},
			"",
			"",
			"txt",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPath, gotName, gotExt := splitFilePath(tt.args.fpath)
			if gotPath != tt.wantPath {
				t.Errorf("splitFilePath() gotPath = %v, want %v", gotPath, tt.wantPath)
			}
			if gotName != tt.wantName {
				t.Errorf("splitFilePath() gotName = %v, want %v", gotName, tt.wantName)
			}
			if gotExt != tt.wantExt {
				t.Errorf("splitFilePath() gotExt = %v, want %v", gotExt, tt.wantExt)
			}
		})
	}
}

var (
	testContent1 = []byte("content1")
	testContent2 = []byte("more content2")
	testContent3 = []byte("a lot more content3")
	testContent4 = []byte("a lot lot lot more content4")
)

func Test_rotateFile(t *testing.T) {
	testCleanDir(testDataDir, t)
	testCreateTestFiles(testDefaultFileDef, t)
	defer testCleanDir(testDataDir, t)

	type args struct {
		path      string
		maxRotate uint
	}
	type content struct {
		src     string
		content string
	}
	tests := []struct {
		name      string
		args      args
		wantErr   bool
		contents  []content
		notExists []string
	}{
		{
			"Test simple rotation",
			args{fmt.Sprintf("testdata%vlog.txt", string(os.PathSeparator)), 3},
			false,
			[]content{
				{
					fmt.Sprintf("testdata%vlog_1.txt", string(os.PathSeparator)),
					string(testContent1),
				},
				{
					fmt.Sprintf("testdata%vlog_2.txt", string(os.PathSeparator)),
					string(testContent2),
				},
				{
					fmt.Sprintf("testdata%vlog_3.txt", string(os.PathSeparator)),
					string(testContent3),
				},
			},
			[]string{
				fmt.Sprintf("testdata%vlog.txt", string(os.PathSeparator)),
				fmt.Sprintf("testdata%vlog_4.txt", string(os.PathSeparator)),
			},
		},
		{
			"Test source file not exist returns error",
			args{fmt.Sprintf("testdata%vlog.log", string(os.PathSeparator)), 3},
			true,
			nil,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Check errors
			if err := rotateFile(tt.args.path, tt.args.maxRotate); (err != nil) != tt.wantErr {
				t.Errorf("rotateFile() error = %v, wantErr %v", err, tt.wantErr)
			}

			// Check content of rotated files
			for _, c := range tt.contents {
				if b, _ := ioutil.ReadFile(c.src); string(b) != c.content {
					t.Errorf("src: %v, b = '%s', wantB '%s'", c.src, b, c.content)
				}
			}

			// Check if files exist
			for _, s := range tt.notExists {
				if stat, err := os.Stat(s); !os.IsNotExist(err) {
					t.Errorf("File exists %v, expected file doesn't exist", stat.Name())
				}
			}
		})
	}

}

// Regression test
func Test_rotateFileBatch(t *testing.T) {
	testCleanDir(testDataDir, t)
	testCreateTestFiles(testDefaultFileDef, t)
	defer testCleanDir(testDataDir, t)

	for i := 0; i < 1000; i++ {
		err := rotateFile(fmt.Sprintf("testdata%vlog.txt", string(os.PathSeparator)), 5)
		if err != nil {
			t.Fatalf("Failed to rotate files -> %v", err)
		}
		testCreateTestFiles([]testFileDef{{"log.txt", []byte("some content doesn't matter")}}, t)
	}
}

func Test_zipFile(t *testing.T) {

	testCleanDir(testDataDir, t)
	testCreateTestFiles(testDefaultFileDef, t)
	defer testCleanDir(testDataDir, t)

	wantPath := fmt.Sprintf("%v%vlog.zip", testDataDir, string(os.PathSeparator))
	path, err := zipFile(fmt.Sprintf("%v%vlog.txt", testDataDir, string(os.PathSeparator)))
	if err != nil {
		t.Fatalf("Compress file failed %v", err)
	}

	if path != wantPath {
		t.Fatalf("path = %v, want %v", path, wantPath)
	}
}

/* Helper functions */
type testFileDef struct {
	name    string
	content []byte
}

var testDefaultFileDef = []testFileDef{
	{"log.txt", testContent1},
	{"log_1.txt", testContent2},
	{"log_2.txt", testContent3},
	{"log_3.txt", testContent4},
}

func testCreateTestFiles(files []testFileDef, t *testing.T) {

	for _, f := range files {
		if err := ioutil.WriteFile(strings.Join([]string{testDataDir, f.name}, string(os.PathSeparator)), f.content, 0666); err != nil {
			t.Fatal(err)
		}
	}
}

func testCleanDir(dir string, t *testing.T) {
	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			t.Fatal(err)
		}
		if info.IsDir() {
			return nil
		}

		if filepath.Base(path) == ".gitignore" {
			return nil
		}

		if err = os.Remove(path); err != nil {
			t.Fatal(err)
			return err
		}
		return nil
	})
}
