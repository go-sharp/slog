package sinks

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"bitbucket.org/go-sharp/slog/shared"
)

// FileSinkOptions configures a new FileSink.
type FileSinkOptions struct {
	// MaxRotate sets how many rotated log files are kept.
	// If set to 0 rotating log file is disabled.
	MaxRotate uint
	// MaxSize limits the size of the log file in KB.
	// If set to 0 no limit is used and rotating the log
	// file is disabled too.
	MaxSize uint
	// LogDate if set to true, every log messeage is prepended
	// with the current time and date.
	LogDate bool
	// LogLevel if set to true, every log message is prepended
	// with the LogLevel of the message.
	LogLevel bool
	// Compress if set to true, rotated log files are stored
	// and packed as a zip file.
	Compress bool
	// DateFormat specifies the format of the date,
	// if LogDate is set to true.
	DateFormat string
	// ExtendedOutput prints additional properties to the output.
	ExtendedOutput bool
}

// NewFileSink creates a new instance of a FileSink.
// Default values used if no FileSinkOptions is provided:
//  - Compress: false
//  - LogDate: true
//  - LogLevel: true
//  - MaxRotate: 0
//  - MaxSize: 0
func NewFileSink(path string, options FileSinkOptions) (shared.Sink, error) {
	path, _ = filepath.Abs(path)

	// Verify if file can be accessed
	f, err := os.OpenFile(path, os.O_CREATE|os.O_RDONLY, 0666)
	if err != nil {
		return nil, err
	}
	if err := f.Close(); err != nil {
		return nil, err
	}

	root, name, ext := splitFilePath(path)
	l := &fileSink{
		mutex:    new(sync.Mutex),
		filePath: root,
		fileName: name,
		fileExt:  ext,
		logDate:  true,
		logLevel: true,
	}

	l.compress = options.Compress
	l.logDate = options.LogDate
	l.logLevel = options.LogLevel
	l.maxRotate = options.MaxRotate
	l.maxSize = options.MaxSize * 1024
	l.dateFormat = options.DateFormat
	l.extendedOutput = options.ExtendedOutput
	if l.dateFormat == "" {
		l.dateFormat = "2006-01-02 15:04:05.000"
	}

	return l, nil
}

// NewFileSinkOptions returns default FileSinkOptions.
// Logs with date and loglevel.
func NewFileSinkOptions() FileSinkOptions {
	return FileSinkOptions{
		LogDate:  true,
		LogLevel: true,
	}
}

// This will force a compiler error if the struct doesn't
// implement the FileSink interface.
var _ shared.Sink = &fileSink{}

type fileSink struct {
	mutex          *sync.Mutex
	maxRotate      uint
	maxSize        uint
	filePath       string
	fileExt        string
	fileName       string
	logDate        bool
	logLevel       bool
	compress       bool
	dateFormat     string
	extendedOutput bool
	fd             *os.File
}

/* Sink interface implementations */
var fbPool = sync.Pool{
	New: func() interface{} {
		return new(bytes.Buffer)
	},
}

func (f *fileSink) WriteEvent(ctx context.Context, event shared.Event) {
	var buf = fbPool.Get().(*bytes.Buffer)
	buf.Reset()
	defer fbPool.Put(buf)

	if f.logDate {
		buf.WriteString("[")
		buf.WriteString(time.Now().Format(f.dateFormat))
		buf.WriteString("] ")
	}

	if f.logLevel {
		buf.WriteString(event.Level.String() + ":")
		buf.WriteString(strings.Repeat(" ", 8-len(event.Level.String())))
	}

	event.Renderer.Render(buf, event.Properties[:event.Renderer.Count()]...)

	// Ensure a minimum distance between message and properties
	buf.WriteString(fillString(buf.Bytes(), " ", 80))

	if f.extendedOutput && len(event.Properties) > 0 {
		for _, p := range event.Properties[:] {
			fmt.Fprintf(buf, "%v=%v|", p.Name, shared.JSONFormat(p))
		}

		if len(event.Properties) > 0 {
			buf.Truncate(buf.Len() - 1)
		}
	}
	buf.WriteString("\n")

	f.mutex.Lock()
	defer f.mutex.Unlock()

	f.checkFile(buf.Len())
	if f.fd == nil {
		return
	}
	f.fd.Write(buf.Bytes())
}

func (f *fileSink) checkFile(msgSize int) {
	if f.fd == nil {
		f.fd = createOrOpen(f.filePath, f.fileName, f.fileExt)
		return
	}

	st, _ := f.fd.Stat()
	if f.maxSize > 0 && st.Size()+int64(msgSize) > int64(f.maxSize) {
		fpath := createPath(f.filePath, f.fileName, f.fileExt)
		f.fd.Close()
		if f.maxRotate > 0 {
			rotatePath := fpath
			if f.compress {
				if p, err := zipFile(fpath); err == nil {
					os.Remove(fpath)
					rotatePath = p
				}
			}
			rotateFile(rotatePath, f.maxRotate)
			f.fd = createOrOpen(f.filePath, f.fileName, f.fileExt)
			return
		}

		var err error
		f.fd, err = os.OpenFile(fpath, os.O_TRUNC|os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			f.fd = getTmpFile(f.fileName, f.fileExt)
		}
	}
}

func createOrOpen(path, name, ext string) *os.File {
	fpath := createPath(path, name, ext)

	_, err := os.Stat(fpath)
	if os.IsPermission(err) {
		return getTmpFile(name, ext)
	}

	if os.IsNotExist(err) {
		writer, err := os.Create(fpath)
		if err != nil {
			return getTmpFile(name, ext)
		}
		return writer
	}

	file, err := os.OpenFile(fpath, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return getTmpFile(name, ext)
	}
	return file
}
