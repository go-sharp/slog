package slog

import (
	"io"

	"bitbucket.org/go-sharp/slog/shared"
)

// WrapLogger wraps a logger into a io.Writer interface. Every write will log a message to the
// underlying logger with the specified log level.
func WrapLogger(l Logger, lvl shared.Level) io.Writer {
	switch lvl {
	case shared.DebugLevel:
		return wrapper(l.Debug)
	case shared.VerboseLevel:
		return wrapper(l.Verbose)
	case shared.WarnLevel:
		return wrapper(l.Warn)
	case shared.ErrorLevel:
		return wrapper(l.Error)
	}
	return wrapper(l.Info)
}

type wrapper func(string, ...interface{})

func (w wrapper) Write(p []byte) (n int, err error) {
	w(string(p))
	return len(p), nil
}
