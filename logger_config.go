package slog

import (
	"context"

	"bitbucket.org/go-sharp/slog/shared"
	"bitbucket.org/go-sharp/slog/sinks"
)

func init() {
	Log = NewLoggerConfig(sinks.NewConsoleSink()).CreateLogger()
}

// Log holds a global Logger instance. It is preconfigured with
// a console sink attached. Setting a new logger should be done
// at the beginning of an application, it is not safe to set
// ir concurrently.
var Log Logger

// LoggerConfig configures logging options and creates a ContextLogger or a Sink.
type LoggerConfig interface {
	AddEnricher(...Enricher) LoggerConfig
	AddSink(...shared.Sink) LoggerConfig
	ControlledBy(LevelSwitch) LoggerConfig
	CreateLogger() Logger
	MinimumLevel(shared.Level) LoggerConfig
	WithProperty(...shared.Property) LoggerConfig
}

// NewLoggerConfig returns a new default log configuration.
func NewLoggerConfig(sinks ...shared.Sink) LoggerConfig {
	var cfg LoggerConfig = &loggerConfig{
		config{
			levelSwitch: NewLevelSwitch(shared.VerboseLevel),
			enrichers:   make([]Enricher, 0, 10),
			properties:  make([]shared.Property, 0, 10),
			sinks:       make([]shared.Sink, 0, 10),
		},
	}
	cfg.AddSink(sinks...)
	return cfg
}

// Enricher enriches an existing log event with additional properties.
type Enricher interface {
	Enrich(context.Context, PropertyAppender)
}

// EnrichFunc wraps a single function into a type
// that implements the Enrich interface.
type EnrichFunc func(context.Context, PropertyAppender)

// Enrich enriches an existing log event with additional properties.
func (e EnrichFunc) Enrich(ctx context.Context, appender PropertyAppender) {
	e(ctx, appender)
}

// PropertyAppender appends a property to an existing log event.
type PropertyAppender func(shared.Property)

type config struct {
	enrichers   []Enricher
	sinks       []shared.Sink
	properties  []shared.Property
	levelSwitch LevelSwitch
}

type loggerConfig struct {
	config
}

var _ LoggerConfig = &loggerConfig{}

func (l *loggerConfig) AddEnricher(enrichers ...Enricher) LoggerConfig {
	l.enrichers = append(l.enrichers, enrichers...)
	return l
}

func (l *loggerConfig) AddSink(sinks ...shared.Sink) LoggerConfig {
	l.sinks = append(l.sinks, sinks...)
	return l
}

func (l *loggerConfig) ControlledBy(ls LevelSwitch) LoggerConfig {
	l.levelSwitch = ls
	return l
}

func (l *loggerConfig) MinimumLevel(lvl shared.Level) LoggerConfig {
	l.levelSwitch = NewLevelSwitch(lvl)
	return l
}

func (l *loggerConfig) WithProperty(props ...shared.Property) LoggerConfig {
	l.properties = append(l.properties, props...)
	return l
}

func (l *loggerConfig) CreateLogger() Logger {
	return newLogger(l.config)
}
