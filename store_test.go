package slog

import (
	"testing"
)

func Test_templateWriter_count(t *testing.T) {
	type args struct {
		template string
	}
	tests := []struct {
		name      string
		args      args
		wantCount int
	}{
		{
			name: "Template without property",
			args: args{
				template: "Only a simple string",
			},
			wantCount: 0,
		},
		{
			name: "Template with two property",
			args: args{
				template: "Only a {user} simple string {myprop} with text",
			},
			wantCount: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ts := newStore()

			got := ts.parseTemplate(tt.args.template)

			if got.writer.Count() != tt.wantCount {
				t.Errorf("templateWriterStore.parseTemplate() count = %v, want %v", got.writer.Count(), tt.wantCount)
			}

		})
	}
}
