package term

import sys "golang.org/x/sys/windows"

// Color describes a Windows console color code sequence.
type Color int8

const (
	// Red Windows console color code for red
	Red Color = 0x0C
	// Blue Windows console color code for blue
	Blue Color = 0x09
	// Green Windows console color code for green
	Green Color = 0x0A
	// Yellow Windows console color code for yellow
	Yellow Color = 0x0E
	// Cyan Windows console color code for cyan
	Cyan Color = 0x0B
	// Magenta Windows console color code for magenta
	Magenta Color = 0x0D
	// Normal Windows console reset color code
	Normal Color = 0x0F
)

var (
	noColor                 bool
	kernel32                *sys.LazyDLL
	getStdHandle            *sys.LazyProc
	setConsoleTextAttribute *sys.LazyProc
	hwd                     uintptr
)

func init() {
	noColor = true
	defer func() {
		// Recover if the dll is not found on the system
		recover()
	}()
	kernel32 = sys.NewLazySystemDLL("Kernel32.dll")
	getStdHandle = kernel32.NewProc("GetStdHandle")
	setConsoleTextAttribute = kernel32.NewProc("SetConsoleTextAttribute")
	if getStdHandle != nil {
		hwd, _, _ = getStdHandle.Call(uintptr(4294967285)) // DWORD - 11 = 2^32 -11
		noColor = hwd < 1
	}
}

// SetColor sets the color of the terminal output.
func SetColor(col Color) {
	if noColor {
		return
	}
	setConsoleTextAttribute.Call(hwd, uintptr(col))
}

// UnsetColor resets to terminal color to normal.
func UnsetColor() {
	if noColor {
		return
	}
	setConsoleTextAttribute.Call(hwd, uintptr(Normal))
}
