// Package term enables to print colored messages to the console.
package term

import (
	"fmt"
	"os"
)

// Print writes the operands to the standard output according
// to the used color constant and resets terminal color
// back to normal. Print's behaviour is analog to fmt.Print command.
func (c Color) Print(a ...interface{}) {
	defer UnsetColor()
	SetColor(c)
	fmt.Fprint(os.Stdout, a...)
}

// Println writes the operands to the standard output according
// to the used color constant and resets terminal color
// back to normal. Println's behaviour is analog to fmt.Println command.
func (c Color) Println(a ...interface{}) {
	defer UnsetColor()
	SetColor(c)
	fmt.Fprintln(os.Stdout, a...)
}

// Printf writes the operands according to a format specifier and the
// used color constant to the standard output. Resets terminal color
// back to normal. Printf's behaviour is analog to fmt.Printf command.
func (c Color) Printf(format string, a ...interface{}) {
	defer UnsetColor()
	SetColor(c)
	fmt.Fprintf(os.Stdout, format, a...)
}
