// +build !windows

package term

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

// Color describes a ANSI color code sequence
type Color string

const (
	// Red ANSI color code for red
	Red Color = "\x1b[31m"
	// Blue ANSI color code for blue
	Blue Color = "\x1b[34m"
	// Green ANSI color code for green
	Green Color = "\x1b[32m"
	// Yellow ANSI color code for yellow
	Yellow Color = "\x1b[33m"
	// Cyan ANSI color code for cyan
	Cyan Color = "\x1b[36m"
	// Magenta ANSI color code for magenta
	Magenta Color = "\x1b[35m"
	// Normal ANSI reset color code
	Normal Color = "\x1b[0m"
)

var noColor bool

func init() {
	t := strings.ToLower(os.Getenv("TERM"))
	dok := false
	if s, err := os.Stdout.Stat(); err == nil {
		dok = s.Mode() == (os.ModeCharDevice & os.ModeDevice)
	}
	ok, _ := regexp.Match("(xterm.*|vt100)", []byte(t))
	// Allow only known terminal with color support
	noColor = !ok && dok
}

// SetColor sets the color of the terminal output
func SetColor(col Color) {
	if noColor {
		return
	}
	fmt.Fprintf(os.Stdout, string(col))
}

// UnsetColor resets to terminal color to normal
func UnsetColor() {
	if noColor {
		return
	}
	fmt.Fprintf(os.Stdout, string(Normal))
}
