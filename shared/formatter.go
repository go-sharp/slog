package shared

import (
	"encoding/json"
	"fmt"
)

// JSONFormat is the default property formatter. If a property's field Serialize is
// set to true, the value will be serialized using the stdlib json.Marshal function.
func JSONFormat(p Property) string {
	if p.Serialize {
		if b, err := json.Marshal(p.Value); err == nil {
			return string(b)
		}
	}
	return fmt.Sprint(p.Value)
}

// Formatter is an adapter to allow the use of ordinary functions as PropertyFormatter.
type Formatter func(Property) string

// Format returns the formatted value of the passed property.
func (f Formatter) Format(p Property) string {
	return f(p)
}
