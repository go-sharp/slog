package shared

import (
	"context"
	"io"
	"time"
)

// Sink is a log event receiver.
type Sink interface {
	WriteEvent(context.Context, Event)
}

// Event is a log message event.
type Event struct {
	Renderer   TemplateRenderer
	Date       time.Time
	Level      Level
	Properties []Property
}

// TemplateRenderer holds the template string and provides the ability
// to write the template to a writer.
type TemplateRenderer interface {
	// Template returns the raw template string.
	Template() string
	// Count returns the number of properties expected for this renderer.
	Count() int
	// Render renders the template string with the supplied properties and writes it to a writer.
	Render(io.Writer, ...Property) error
	// RenderF same as Write but accepts a property formatter
	RenderF(io.Writer, PropertyFormatter, ...Property) error
}

// PropertyFormatter formats a property to a string.
type PropertyFormatter interface {
	Format(Property) string
}

// Property enriches a log event.
type Property struct {
	// Name is the name of the property.
	Name string
	// Value is a struct, slice of struct or a primitive data type like int, bool, etc...
	Value interface{}
	// Serialize indicates if the value should be serialized
	// or if only the string representation should be used.
	Serialize bool
	// Format specifies how a value of the property should be formatted.
	Format string
	// Alignment defines how a value of the property should be aligned.
	Alignment string
}

// Level is the loglevel of an log event.
type Level int

func (l Level) String() string {
	switch l {
	case VerboseLevel:
		return "Verbose"
	case DebugLevel:
		return "Debug"
	case InfoLevel:
		return "Info"
	case WarnLevel:
		return "Warning"
	case ErrorLevel:
		return "Error"
	case FatalLevel:
		return "Fatal"
	default:
		return ""
	}
}

const (
	// VerboseLevel traceing and debugging messages
	VerboseLevel Level = 1 << iota
	// DebugLevel debugging messages
	DebugLevel
	// InfoLevel informational messages
	InfoLevel
	// WarnLevel non critical error messages
	WarnLevel
	// ErrorLevel non recoverable error messages
	ErrorLevel
	// FatalLevel a terminating error messages
	FatalLevel
	// NoneLevel no messages should be logged
	NoneLevel
)
