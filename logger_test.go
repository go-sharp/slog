package slog

import (
	"bytes"
	"context"
	"reflect"
	"sync"
	"testing"

	"bitbucket.org/go-sharp/slog/shared"
)

func Test_logger_logEvent(t *testing.T) {
	var debugLvl LevelSwitch = &levelSwitch{lvl: shared.DebugLevel, mux: new(sync.RWMutex)}
	enricher := EnrichFunc(func(ctx context.Context, appender PropertyAppender) {
		appender(shared.Property{Name: "Enrich1", Value: "Value1"})
		appender(shared.Property{Name: "Enrich1", Value: "Value2"})
	})
	type fields struct {
		cfg   *config
		store *templateWriterStore
		props []shared.Property
	}
	type args struct {
		ctx   context.Context
		lvl   shared.Level
		msg   string
		props []interface{}
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		count     int
		wantEvent shared.Event
		wantMsg   string
		dontLog   bool
	}{
		{
			name: "Simple log entry",
			fields: fields{
				cfg: &config{
					properties:  []shared.Property{{Name: "Test", Value: 42}},
					levelSwitch: &levelSwitch{lvl: shared.VerboseLevel, mux: new(sync.RWMutex)},
				},
				store: newStore(),
				props: []shared.Property{{Name: "Test2", Value: "Hello"}},
			},
			args: args{
				lvl:   shared.InfoLevel,
				msg:   "User {User} logged in from {IP}",
				props: []interface{}{"jbond", "127.0.0.1"},
			},
			wantEvent: shared.Event{
				Level: shared.InfoLevel,
				Properties: []shared.Property{
					{Name: "User", Value: "jbond"},
					{Name: "IP", Value: "127.0.0.1"},
					{Name: "Test", Value: 42},
					{Name: "Test2", Value: "Hello"},
				},
			},
			count:   2,
			wantMsg: "User jbond logged in from 127.0.0.1",
		},
		{
			name: "Log entry with missing prop",
			fields: fields{
				cfg: &config{
					properties:  []shared.Property{{Name: "Test", Value: 42}},
					levelSwitch: &levelSwitch{lvl: shared.VerboseLevel, mux: new(sync.RWMutex)},
				},
				store: newStore(),
				props: []shared.Property{{Name: "Test2", Value: "Hello"}},
			},
			args: args{
				lvl:   shared.InfoLevel,
				msg:   "User {User} logged in from {IP}",
				props: []interface{}{"jbond"},
			},
			wantEvent: shared.Event{
				Level: shared.InfoLevel,
				Properties: []shared.Property{
					{Name: "User", Value: "jbond"},
					{Name: "IP", Value: nil},
					{Name: "Test", Value: 42},
					{Name: "Test2", Value: "Hello"},
				},
			},
			count:   2,
			wantMsg: "User jbond logged in from <nil>",
		},
		{
			name: "Log entry with more props",
			fields: fields{
				cfg: &config{
					properties:  []shared.Property{{Name: "Test", Value: 42}},
					levelSwitch: &levelSwitch{lvl: shared.VerboseLevel, mux: new(sync.RWMutex)},
				},
				store: newStore(),
				props: []shared.Property{{Name: "Test2", Value: "Hello"}},
			},
			args: args{
				lvl: shared.InfoLevel,
				msg: "User {User} logged in from {IP}",
				props: []interface{}{"jbond", "127.0.0.1", struct {
					msg  string
					age  int
					addr struct {
						street string
						plz    int
					}
				}{
					msg: "Nope",
					age: 42,
					addr: struct {
						street string
						plz    int
					}{
						street: "Bahnhofstrasse",
						plz:    8000,
					},
				}},
			},
			wantEvent: shared.Event{
				Level: shared.InfoLevel,
				Properties: []shared.Property{
					{Name: "User", Value: "jbond"},
					{Name: "IP", Value: "127.0.0.1"},
					{Name: "Unused values", Value: []interface{}{
						struct {
							msg  string
							age  int
							addr struct {
								street string
								plz    int
							}
						}{
							msg: "Nope",
							age: 42,
							addr: struct {
								street string
								plz    int
							}{
								street: "Bahnhofstrasse",
								plz:    8000,
							},
						},
					}},
					{Name: "Test", Value: 42},
					{Name: "Test2", Value: "Hello"},
				},
			},
			count:   2,
			wantMsg: "User jbond logged in from 127.0.0.1",
		},
		{
			name: "Log entry with level not logged",
			fields: fields{
				cfg: &config{
					properties:  []shared.Property{{Name: "Test", Value: 42}},
					levelSwitch: debugLvl,
				},
				store: newStore(),
				props: []shared.Property{{Name: "Test2", Value: "Hello"}},
			},
			args: args{
				lvl:   shared.VerboseLevel,
				msg:   "User {User} logged in from {IP}",
				props: []interface{}{"jbond", "127.0.0.1"},
			},
			wantEvent: shared.Event{
				Level: shared.VerboseLevel,
				Properties: []shared.Property{
					{Name: "User", Value: "jbond"},
					{Name: "IP", Value: "127.0.0.1"},
					{Name: "Test", Value: 42},
					{Name: "Test2", Value: "Hello"},
				},
			},
			count:   2,
			wantMsg: "User jbond logged in from 127.0.0.1",
			dontLog: true,
		},
		{
			name: "Log entry with enricher",
			fields: fields{
				cfg: &config{
					properties:  []shared.Property{{Name: "Test", Value: 42}},
					levelSwitch: &levelSwitch{lvl: shared.VerboseLevel, mux: new(sync.RWMutex)},
					enrichers:   []Enricher{enricher},
				},
				store: newStore(),
				props: []shared.Property{{Name: "Test2", Value: "Hello"}},
			},
			args: args{
				lvl:   shared.InfoLevel,
				msg:   "User {User} logged in from {IP}",
				props: []interface{}{"jbond", "127.0.0.1"},
			},
			wantEvent: shared.Event{
				Level: shared.InfoLevel,
				Properties: []shared.Property{
					{Name: "User", Value: "jbond"},
					{Name: "IP", Value: "127.0.0.1"},
					{Name: "Test", Value: 42},
					{Name: "Test2", Value: "Hello"},
					{Name: "Enrich1", Value: "Value1"},
				},
			},
			count:   2,
			wantMsg: "User jbond logged in from 127.0.0.1",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var gotEvent *shared.Event

			sink := SinkFunc(func(ctx context.Context, e shared.Event) {
				gotEvent = &e
			})

			tt.fields.cfg.sinks = append(tt.fields.cfg.sinks, sink)

			l := &logger{
				cfg:   tt.fields.cfg,
				store: tt.fields.store,
				props: tt.fields.props,
			}
			l.logEvent(tt.args.ctx, tt.args.lvl, tt.args.msg, tt.args.props...)

			if tt.dontLog != (gotEvent == nil) {
				t.Errorf("logger.logEvent dontLog = %v, want %v", tt.dontLog, (gotEvent == nil))
				return
			} else if gotEvent == nil {
				return
			}

			if !reflect.DeepEqual(gotEvent.Level, tt.wantEvent.Level) {
				t.Errorf("logger.logEvent got level = %v, want %v", gotEvent.Level, tt.wantEvent.Level)
				return
			}

			if !reflect.DeepEqual(gotEvent.Properties, tt.wantEvent.Properties) {
				t.Errorf("logger.logEvent got properties = %v, want %v", gotEvent.Properties, tt.wantEvent.Properties)
				return
			}
			var buf bytes.Buffer
			gotEvent.Renderer.Render(&buf, gotEvent.Properties[:tt.count]...)
			if tt.wantMsg != buf.String() {
				t.Errorf("logger.logEvent got msg = %v, want %v", buf.String(), tt.wantMsg)
			}
		})
	}
}
